from datetime import datetime, timedelta
import unittest
from app import app, db
from app.models import User, Movie


class UserModelCase(unittest.TestCase):
    def setUp(self):
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_password_hashing(self):
        u = User(username='somerandomuser')
        u.set_password('mypassword')
        self.assertFalse(u.check_password('notmypassword'))
        self.assertTrue(u.check_password('mypassword'))

    def test_vote_movie(self):
        u1 = User(username='user1')
        u2 = User(username='user2')
        u3 = User(username='user3')
        db.session.add_all([u1, u2, u3])

        now = datetime.utcnow()
        m1 = Movie(name='movie1', submitted_by=u1, date_added=now, description='some desc of movie1')
        m2 = Movie(name='movie2', submitted_by=u1, date_added=now, description='some desc of movie2')
        m3 = Movie(name='movie3', submitted_by=u2, date_added=now, description='some desc of movie3')
        m4 = Movie(name='movie4', submitted_by=u1, date_added=now, description='some desc of movie4')
        db.session.add_all([m1, m2, m3, m4])
        db.session.commit()

        m1.liked(u1)
        m1.liked(u2)
        m1.hated(u3)
        m2.liked(u3)
        m2.hated(u2)
        m3.liked(u1)
        m3.liked(u3)
        db.session.commit()

        self.assertFalse(m1.is_liked(u1))
        self.assertTrue(m1.is_liked(u2))
        self.assertTrue(m1.is_hated(u3))
        self.assertTrue(m2.is_liked(u3))
        self.assertTrue(m2.is_hated(u2))
        self.assertTrue(m3.is_liked(u1))
        self.assertTrue(m3.is_liked(u3))
        self.assertEqual(m1.number_of_likes, 1)
        self.assertEqual(m2.number_of_likes, 1)
        self.assertEqual(m3.number_of_likes, 2)
        self.assertEqual(m1.number_of_hates, 1)
        self.assertEqual(m2.number_of_hates, 1)

        m1.unliked(u2)
        m1.unhated(u3)
        m2.liked(u2)
        db.session.commit()

        self.assertFalse(m1.is_liked(u2))
        self.assertFalse(m1.is_hated(u3))
        self.assertEqual(m1.number_of_likes, 0)
        self.assertEqual(m1.number_of_hates, 0)
        self.assertTrue(m2.is_liked(u2))
        self.assertEqual(m2.number_of_likes, 2)
        self.assertEqual(m2.number_of_hates, 0)


if __name__ == '__main__':
    unittest.main(verbosity=2)
