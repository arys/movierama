from app import app, db
from app.forms import LoginForm, RegistrationForm, MovieSubmissionForm
from app.models import User, Movie
from werkzeug.urls import url_parse
from flask import render_template, flash, redirect, url_for, request
from flask_login import current_user, login_user, logout_user, login_required
from datetime import datetime
from sqlalchemy.sql.expression import desc, asc


def define_sort_oder(sort_by):
    sort_options = ('date', 'like', 'hate')
    sort_fields = ('date_added', 'number_of_likes', 'number_of_hates')
    if sort_by not in sort_options:
        sort_by = 'date'
    order_by_option = "Movie." + sort_fields[sort_options.index(sort_by)]
    return order_by_option


@app.template_filter()
def timesince(dt, default="just now"):
    """
    Returns string representing "time since" e.g.
    3 days ago, 5 hours ago etc.
    Taken from flask documentation. http://flask.pocoo.org/snippets/33/
    """

    now = datetime.utcnow()
    diff = now - dt
    periods = (
        (diff.days, "day", "days"),
        (diff.seconds / 3600, "hour", "hours"),
        (diff.seconds / 60, "minute", "minutes"),
        (diff.seconds, "second", "seconds"),
    )

    for period, singular, plural in periods:

        if period:
            return "%d %s ago" % (period, singular if period == 1 else plural)

    return default


@app.template_filter()
def number_of_votes(votes, type, movie_id):
    if votes is None:
        return "0 %s" % (type)
    elif votes == 1:
        return "1 %s" % ("like" if type == 'likes' else 'hate')
    else:
        return "%d %s" % (votes, type)


@app.template_filter()
def liked_or_hated(movie_id):
    user = User.query.filter_by(username=current_user.username).first()
    movie = Movie.query.filter_by(id=movie_id).first()
    if movie.is_liked(user):
        return "Liked already"
    if movie.is_hated(user):
        return "Hated movie"
    return "Never voted"

@app.route('/')
@app.route('/index')
def index():
    sort_by = request.args.get('sort_by', default='date')
    sort_order = request.args.get('order', default='desc')
    movies = Movie.query.order_by(asc(define_sort_oder(sort_by))) if sort_order == 'asc' else \
        Movie.query.order_by(desc(define_sort_oder(sort_by)))
    return render_template('index.html', title='MoveRama', movies=movies)


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    movies = user.submissions
    if movies is None:
        flash('User has no movies submitted.')
    return render_template('user.html', user=user, movies=movies)


@app.route('/add_movie', methods=['GET', 'POST'])
@login_required
def add_movie():
    form = MovieSubmissionForm()
    if form.validate_on_submit():
        userid = User.query.filter_by(username=current_user.username).first()
        movie = Movie(submitted_by=userid , name=form.name.data, description=form.description.data)
        db.session.add(movie)
        db.session.commit()
        flash('Movie ' + form.name.data + ' added successfully.')
        return redirect(url_for('login'))
    return render_template('movies.html', title='Add Movie', form=form)


@app.route('/vote/<username>')
@login_required
def vote(username):
    movie_id = request.args.get('movie_id', default=None)
    vote_type = request.args.get('vote_type', default=None)
    remove_vote = request.args.get('remove_vote', default=None)
    user = User.query.filter_by(username=username).first()
    movie = Movie.query.filter_by(id=movie_id).first()
    if user.id == movie.submitted_by.id:
        flash('You cannot vote for a movie you submitted.')
        return redirect(request.referrer)
    if vote_type == 'like':
        if remove_vote == 'yes':
            movie.unliked(user)
        elif remove_vote is None:
            movie.liked(user)
    if vote_type == 'hate':
        if remove_vote == 'yes':
            movie.unhated(user)
        elif remove_vote is None:
            movie.hated(user)
    db.session.commit()
    return redirect(request.referrer)
