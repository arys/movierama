from app import db, login
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin


likes_association_table = db.Table('likes_table',
                                   db.Column('user_id', db.Integer, db.ForeignKey('user.id'), nullable=False),
                                   db.Column('movie_id', db.Integer, db.ForeignKey('movie.id'), nullable=False),
                                   db.PrimaryKeyConstraint('user_id', 'movie_id')
                                   )


hates_association_table = db.Table('hates_table',
                                   db.Column('user_id', db.Integer, db.ForeignKey('user.id'), nullable=False),
                                   db.Column('movie_id', db.Integer, db.ForeignKey('movie.id'), nullable=False),
                                   db.PrimaryKeyConstraint('user_id', 'movie_id')
                                   )


class User(UserMixin, db.Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    submissions = db.relationship('Movie', backref='submitted_by', lazy='dynamic')
    likes = db.relationship("Movie",
                            secondary=likes_association_table,
                            backref="liked_by",
                            lazy='dynamic')
    hates = db.relationship("Movie",
                            secondary=hates_association_table,
                            backref="hated_by",
                            lazy='dynamic')
    expr = db.query_expression()

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


@login.user_loader
def load_user(id):

    return User.query.get(int(id))


class Movie(db.Model):
    __tablename__ = 'movie'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), index=True, unique=True)
    added_by = db.Column(db.Integer, db.ForeignKey('user.id'))
    date_added = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    description = db.Column(db.String(255))
    number_of_likes = db.Column(db.Integer, default=0)
    number_of_hates = db.Column(db.Integer, default=0)
    expr = db.query_expression()

    def liked(self, user):
        if (not self.is_liked(user)) and (self.submitted_by.id != user.id):
            self.liked_by.append(user)
            self.number_of_likes = 0 if self.number_of_likes is None else self.number_of_likes + 1
            if self.is_hated(user):
                self.unhated(user)

    def hated(self, user):
        if (not self.is_hated(user)) and (self.submitted_by.id != user.id):
            self.hated_by.append(user)
            self.number_of_hates = 0 if self.number_of_hates is None else self.number_of_hates + 1
            if self.is_liked(user):
                self.unliked(user)

    def unliked(self, user):
        if self.is_liked(user):
            self.liked_by.remove(user)
            self.number_of_likes = 0 if self.number_of_likes == 0 else  self.number_of_likes - 1

    def unhated(self, user):
        if self.is_hated(user):
            self.hated_by.remove(user)
            self.number_of_hates = 0 if self.number_of_hates == 0 else self.number_of_hates - 1

    def is_liked(self, user):
        return user.likes.filter(likes_association_table.c.movie_id == self.id).count() > 0

    def is_hated(self, user):
        return user.hates.filter(hates_association_table.c.movie_id == self.id).count() > 0
