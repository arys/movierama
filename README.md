# MovieRama

The app is written in Python 3 using the Flask framework.
All packages are listed under requirements.txt

It is advised to use a virtual environment for the project and install the requirements there.
```bash
$ python3 -m venv mrenv
$ source mrenv/bin/activate
$ pip install -r requirements.txt
``` 
Once this environment is ready you may initialize the database. SQLite is used for this simple PoC.
```bash
$ flask db upgrade
```
After these steps you may run the application.
```bash
$ python movierama.py
```

You may also choose to run behind an HTTP server such Gunicorn or uWSGI. 
This is an example using Gunicorn running on port 5000.
```bash
$ pip install gunicorn
$ gunicorn  -b :5000 --access-logfile - --error-logile - movierama:app
```


An even easier way is using the avaiable Docker image at arys/movierama:latest
```sh
$ docker run --name movierama -d -p80:5000 --rm arys/movierama:latest
```
This will start MovieRama in a Docker container running on port 80.

## Heroku Deployment
A demo server hosted on Heroku is also available at this link: 
[https://movierama-arys.herokuapp.com/](https://movierama-arys.herokuapp.com/)

The following three users have already been created along with some movies:

| Username  | Password  |
|---|---|
|user1|user1|
|user2|user2
|user3|user3|
