FROM python:3.6-alpine

RUN adduser -D movierama

WORKDIR /home/movierama

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn

COPY app app
COPY migrations migrations
COPY movierama.py config.py boot.sh ./
RUN chmod +x boot.sh

ENV FLASK_APP movierama.py

RUN chown -R movierama:movierama ./
USER movierama

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]
